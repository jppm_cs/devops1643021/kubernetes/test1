from fastapi import FastAPI, HTTPException
from models import User  # Assuming you have a User model defined in "models.py"
import os
import uvicorn
from fastapi.middleware.cors import CORSMiddleware
import mysql.connector as mysql


app = FastAPI()

# allowed_origins = ["*"]
# app.add_middleware(
#     CORSMiddleware,
#     allowed_origins=allowed_origins,
#     allow_credentials=True,
#     allow_methods=["*"],
#     allow_headers=["*"],
# )
# Create a mysql connection pool instead of a single connection and cursor

mysql_user = os.environ.get("MYSQL_USER")
mysql_host = os.environ.get("MYSQL_HOST")
mysql_password = os.environ.get("MYSQL_PASSWORD")
mysql_database = os.environ.get("MYSQL_DATABASE")
print(mysql_user,mysql_database,mysql_password)
conn = mysql.connect(
    user=mysql_user,
    password=mysql_password,
    host=mysql_host,
    database=mysql_database,
)


# Define a route for creating a user
@app.post("/api/user")
def create_user(user: User):
    pass


# Define a route for reading users
@app.get("/api/user")
def read_users():
    return {"working": "is working"}


# Define a route for deleting a user by ID
@app.delete("/api/user/{user_id}")
def delete_user(user_id: int):
    pass


if __name__ == "__main__":
    config = uvicorn.Config("main:app", port=5000, log_level="info", host="0.0.0.0")
    server = uvicorn.Server(config)
    server.run()
