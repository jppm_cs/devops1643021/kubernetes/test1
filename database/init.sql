CREATE TABLE users (
  id INT AUTO_INCREMENT NOT NULL,
  name VARCHAR(255) NOT NULL,
  secret VARCHAR(255) NOT NULL,
  PRIMARY KEY (id)
);
INSERT INTO users (name, secret) VALUES ("juan", "juan 2.0");
INSERT INTO users (name, secret) VALUES ("daniel", "daniel 2.0");

